import React from 'react';
import introImg from './intro.JPG';
import IdeaFeed from '../../components/common/IdeaFeed';
import Skelton from '../../components/common/Skelton';
import { Header } from 'semantic-ui-react';
import axios from 'axios';

class WhatsBuilt extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      feedData: null
    };
  }

  componentDidMount() {
    const getApiUrl = 'stub/get.json';

    // connect to API
    axios.get(getApiUrl).then(res => {
      this.setState({
        feedData: res.data,
        isLoading: false,
      })
    });
  }

  /**
   * Method will filter the data retreived from API
   * based on status node
   * for 'What's in-progress' section.
  */
  filterInProgressIdeas() {
    const suggestions =Object.assign([], this.state.feedData);
    const inProgressIdeas = suggestions.filter(function(item) {
      return item.status === 'in-progress';
    });

    return inProgressIdeas;
  }

  /**
   * Method will filter the data retreived from API
   * based on status node
   * for 'What's in-progress' section.
  */
 filterImplimentedIdeas() {
  const suggestions =Object.assign([], this.state.feedData);
  const implimentedIdeas = suggestions.filter(function(item) {
    return item.status === 'implimented';
  });

  return implimentedIdeas;
  }


  render() {
    const { isLoading } = this.state;
    const inProgressIdeas = !isLoading ? this.filterInProgressIdeas() : null;
    const implimentedIdeas = !isLoading ? this.filterImplimentedIdeas() : null;

    return (
      <div className="container-box">
        <h3 className="ui red header">Progress</h3>
        {/* Intro section */}
        <div className="float-left intro-image">
          <img src={introImg} alt="Get Idea" className="responsive-image" />
        </div>
        {/* -- intro */}
        <div className="float-right intro-text">
          <h4>Whats getting built</h4>
          <p>Here you can see which of the submitted ideas are in our backlog and which have already been deployed.</p>
          <p>Every good idea will end up here eventually.</p>
        </div>
        <div className="clear-float"></div>

        <Header as='h4' color='grey'>Whats been implimented?</Header>
        {!isLoading ? (
          <IdeaFeed 
                feeds={implimentedIdeas}
                loading={this.state.loading}
          />
        ) : (
          <Skelton />
        )}

        <Header as='h4' color='grey'>Whats in progress?</Header>
        {!isLoading ? (
          <IdeaFeed 
                feeds={inProgressIdeas}
                loading={this.state.loading}
          />
        ) : (
          <Skelton />
        )}

      </div>
    );
  }
}
export default WhatsBuilt;
