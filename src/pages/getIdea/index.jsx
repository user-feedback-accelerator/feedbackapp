import React from 'react';
import introImg from './intro.JPG';
import IdeaFeed from '../../components/common/IdeaFeed';
import Skelton from '../../components/common/Skelton';
import { Header } from 'semantic-ui-react';
import axios from 'axios';

class GetIdea extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      feedData: null
    }
  }

  componentDidMount() {
    const getApiUrl = 'stub/get.json';

    // connect to API
    axios.get(getApiUrl).then(res => {
      this.setState({
        feedData: res.data,
        isLoading: false,
      })
    });
  }

  /**
   * Method will sort the data retreived from API
   * based on number of likes
   * for 'Top Ideas' section.
  */
  sortForTopIdeas() {
    const suggestions =Object.assign([], this.state.feedData);
    const topIdeas = suggestions.sort(function(a, b) {
      var itemA = a.likes ? a.likes.length : 0;
      var itemB = b.likes ? b.likes.length : 0;
      return itemB - itemA ;
    });

    return topIdeas;
  }

  /**
   * Method will sort the data retreived from API
   * based on createdDateTime
   * for 'Most Recent Ideas' section.
  */
  sortForRecentIdeas() {
    const suggestions =Object.assign([], this.state.feedData);
    const recentIdeas = suggestions.sort(function(a, b) {
      return new Date(b.createdDateTime) - new Date(a.createdDateTime);
    });

    return recentIdeas;
  }

  render() {
    const { isLoading } = this.state;
    const topIdeas = !isLoading ? this.sortForTopIdeas() : null;
    const recentIdeas = !isLoading ? this.sortForRecentIdeas() : null;

    return (
      <div className="container-box">
        <h3 className="ui red header">Ideas</h3>
        {/* Intro section */}
        <div className="float-left intro-image">
          <img src={introImg} alt="Get Idea" className="responsive-image" />
        </div>
        {/* -- intro text */}
        <div className="float-right intro-text">
          <h4>What's in the lead?</h4>
          <p>You can see the ideas have had the most up votes and are currently the front runners for us to build.
          </p>
          <p>Vote for your favourite ideas and see them added to your applications and tools! </p>
        </div>
        <div className="clear-float"></div>

        {/* Top Ideas */}
        <Header as='h4' color='grey'>Top Ideas</Header>
        {!isLoading ? (
          <IdeaFeed 
                feeds={topIdeas}
                loading={this.state.loading}
          />
        ) : (
          <Skelton />
        )}

         {/* Most Recent Ideas */}
        <Header as='h4' color='grey'>Most Recent Ideas</Header>
        {!isLoading ? (
          <IdeaFeed 
                feeds={recentIdeas}
                loading={this.state.loading}
          />
        ) : (
          <Skelton />
        )}
       
      </div>
    );
  }
}
export default GetIdea;
