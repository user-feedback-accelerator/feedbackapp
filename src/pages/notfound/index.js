import React from 'react';

class Notfound extends React.Component {
    render() {
        return(
            <div className="container-box">
                <h1 className="ui red header">404</h1>
                <h3 className="ui red header">Page Not found!</h3>
            </div>
        )
    }
}
export default Notfound;