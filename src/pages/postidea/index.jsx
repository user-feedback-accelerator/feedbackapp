import React, { Component } from 'react';
import IdeaFeed from '../../components/common/IdeaFeed';
import Skelton from '../../components/common/Skelton';
import introImg from './intro.JPG';
import {
  Button,
  Select,
  Form,
  Message,
  Input,
  TextArea,
  Divider
} from 'semantic-ui-react';
import axios from 'axios';

const suggestionTypes = [
  { text: 'Improvement', value: 'improvement' },
  { text: 'Observation', value: 'observation' },
  { text: 'New Feature', value: 'newfeature' }
];
const applications = [
  { text: 'SAMM', value: 'SAMM' },
  { text: 'Colleagues on the go', value: 'colleagues' }
];

class PostIdea extends Component {
  constructor(props) {
    super(props);
    this.state = {
      suggestionType: '',
      application: '',
      title: '',
      suggestion: '',
      createdBy: 'Anonymous',
      isLoading: true,
      isTitleQuery: false,
      typingTimeout: 0,
      feedData: null
    };

    this.handleChange = this.handleChange.bind(this);
    this.handlePost = this.handlePost.bind(this);
  }

  /**
   * Method is called on change event of title field.
   * It will connect to API to get matched feedback.
  */
  handleTitleChange = (e) => {
    const self = this;

    if (this.state.typingTimeout) {
      clearTimeout(this.state.typingTimeout);
    }

    this.setState({
      title: e.target.value,
      isTitleQuery: true,
      typingTimeout: setTimeout(function () {
            self.getFeedbacks()
      }, 5000)
    });
  }

  /**
   * called from handleTitleChange
   * it will connect to API and then save searched data to state.
  */
  getFeedbacks() {
    const getApiUrl = 'stub/get.json?prefix=?' + this.state.title + '&limit=7';

    // connect to API
    axios.get(getApiUrl).then(res => {
      this.setState({
        feedData: res.data,
        isLoading: false
      })
    });
  }

  /**
   * Method will handle the change of form fields
   * to maintain the state data
   */
  handleChange = (e, { name, value }) => this.setState({ [name]: value });

  /**
   * called from the POST button
   * of form
   */
  handlePost(event) {
    event.preventDefault();

    this.postToSlack();
    this.addFeedbackToDatabase();
    alert('Idea is successfully posted.');
    this.clearState();
  }

  /**
   * called from handlePost()
   * Method will connect to API to add feedback message to database
   */
  addFeedbackToDatabase() {
    const feedbackApiUrl = 'https://jsonplaceholder.typicode.com/users';
    const feedbackApiRequest = {
      suggestionType: this.state.suggestionType,
      application: this.state.application,
      title: this.state.title,
      suggestion: this.state.suggestion,
      createdBy: this.state.createdBy
    };

    // connect to API
    axios.post(feedbackApiUrl, feedbackApiRequest).then(res => {
      console.log(res);
    });
  }

  /**
   * called from handlePost()
   * Method will connect to slack API to post message
   */
  postToSlack() {
    const slackUrl =
      'https://hooks.slack.com/services/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';
    const message =
      this.state.suggestionType +
      ' / ' +
      this.state.application +
      '\n' +
      this.state.title +
      '\n' +
      this.state.suggestion;
    const slackMessage = {
      text: message
    };
    // slack is making CORS issue - this is workaround for the same
    axios.post(slackUrl, JSON.stringify(slackMessage), {
      withCredentials: false,
      transformRequest: [
        (slackMessage, headers) => {
          delete headers.post['Content-Type'];
          return slackMessage;
        }
      ]
    });
  }

  /**
   * Method will clear the state data
   * once feedback is posted
   */
  clearState() {
    this.setState({
      suggestionType: '',
      application: '',
      title: '',
      suggestion: ''
    });
  }

  render() {
    const { isLoading } = this.state;
    const { isTitleQuery } = this.state;
    
    return (
      <div className="container-box">
        <h3 className="ui red header">Ideas</h3>
        {/* Intro section */}
        <div className="float-left intro-image">
          <img src={introImg} alt="Idea" className="responsive-image" />
        </div>
        <div className="float-right intro-text">
          <h4>We would love to hear from you!</h4>
          <p>
            If you have an idea to improve anything you use day to day, we'd
            love to learn more and see what we can do to help.
          </p>
          <p>
            You can view others ideas and how they are doing in the other tabs.
          </p>
          <p>Help build your tools in a way that nest suits you!</p>
        </div>
        <div className="clear-float">&nbsp;</div>

        {/* Form section */}
        <Form>
          <Form.Field>
            <Form.Field
              control={Select}
              label="What Kind of Idea do you have?"
              name="suggestionType"
              value={this.state.suggestionType}
              onChange={this.handleChange}
              options={suggestionTypes}
            />
            <Form.Field
              control={Select}
              label="What application or tool you want to help within?"
              name="application"
              value={this.state.application}
              onChange={this.handleChange}
              options={applications}
            />
            <Form.Field
              control={Input}
              label="Whats the title of your idea"
              name="title"
              value={this.state.title}
              onChange={this.handleTitleChange}
            />
            
            {/* ideas on user typing */}
            {!isLoading ? (
              <div>
                <Message
                  info
                  content="Here are some matching ideas, you may be intrested in, Otherwise post a new idea."
                />
                <IdeaFeed 
                    feeds={this.state.feedData}
                    loading={this.state.loading}
                />
                </div>
            ) : (
              isTitleQuery ? <Skelton /> : ''
            )}

            <Divider />

            <Form.Field
              control={TextArea}
              label="Your Idea"
              name="suggestion"
              value={this.state.suggestion}
              onChange={this.handleChange}
            />
          </Form.Field>

          {/* footer post button section */}
          <br />
          <div className="float-left footer-grey-text">
            <p>
              You can see how your idea is doing along with the others on the
              next tab...
            </p>
          </div>
          <div className="float-right">
            <Button
              primary
              content="Post"
              onClick={this.handlePost}
              type="submit"
            />
          </div>
          <div className="cleat-float">&nbsp;</div>
          <br />
        </Form>
      </div>
    );
  }
}

export default PostIdea;
