import React from 'react';
import Like from './Like';
import renderer from 'react-test-renderer';

test('Like changes the text and color when cicked', () => {
  
  const component = renderer.create(
    <Like />
  )
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();

  // onClick()
  tree.props.onClick();
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();

});