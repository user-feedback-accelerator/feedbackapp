import React, { Component } from 'react';
import { Feed, Icon } from 'semantic-ui-react';

class Like extends Component {

  constructor(props) {
    super(props);
    let likes = this.props.totalLikes ? this.props.totalLikes : 0;
    let dislikes = this.props.totalDisLikes ? this.props.totalDisLikes : 0;
    this.state = { 
        liked: false,
        disliked: false,
        likesCounter: likes,
        dislikesCounter: dislikes
    };
    this.handleLike = this.handleLike.bind(this);
    this.handleDislike = this.handleDislike.bind(this);
  }

  /**
   * Method will handle the like logic
   * On click it will check for like/unlike and accordingly
   * it will increase or decrease the like counts.
   * 
   * In future, it will connect to api to send like
   * record to database
  */
  handleLike(event) {
    this.setState({ liked: !this.state.liked });
    if(this.state.liked) {
        this.setState({ likesCounter: this.state.likesCounter - 1 }); 
    } else {
        this.setState({ likesCounter: this.state.likesCounter + 1 }); 
    }

    // toggle dislike and decrese dislike counter if selected
    if(this.state.disliked) {
      this.setState({ 
        dislikesCounter: this.state.dislikesCounter - 1,
        disliked: false
      }); 
    }
  }

  /**
   * Method will handle the dislike logic
   * On click it will check for like/unlike and accordingly
   * it will increase or decrease the dislike counts.
   * 
   * In future, it will connect to api to send dilike
   * record to database
  */
  handleDislike(event) {
    this.setState({ disliked: !this.state.disliked });
    if(this.state.disliked) {
        this.setState({ dislikesCounter: this.state.dislikesCounter - 1 }); 
    } else {
        this.setState({ dislikesCounter: this.state.dislikesCounter + 1 }); 
    }

    // toggle like and decrese like counter if selected
    if(this.state.liked) {
      this.setState({ 
        likesCounter: this.state.likesCounter - 1,
        liked: false
      }); 
    }
  }

  render() {
    let likeIconColor = this.state.liked? 'red': 'grey';
    let dislikeIconColor = this.state.disliked? 'red': 'grey';
    return (
        <div>
          <Feed.Like onClick={this.handleLike}>
            <Icon name="thumbs up" color={likeIconColor} /> {this.state.likesCounter}
          </Feed.Like>
          &nbsp; &nbsp; 
          <Feed.Like onClick={this.handleDislike}>
            <Icon name="thumbs down" color={dislikeIconColor} /> {this.state.dislikesCounter}
          </Feed.Like>
          &nbsp; &nbsp; 
          <a href="#">comment</a>
        </div>
    );
  }
}

export default Like;
