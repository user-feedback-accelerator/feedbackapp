import React, { Component } from 'react';
import Like from './Like';
import Skelton from './Skelton';
import { Feed } from 'semantic-ui-react';

class IdeaFeed extends Component {
 
  render() {
    const { isLoading } = this.props;

    const {feeds} = this.props
    const feed = !isLoading ? feeds.map((feed, i) =>
      <Feed.Event key={i}>
        <Feed.Label image="/images/helen.jpg" />
        <Feed.Content>
          <Feed.Summary>
            <Feed.User>{feed.createdBy}</Feed.User>
            <Feed.Date>
              {new Intl.DateTimeFormat('en-GB', { 
                year: 'numeric', 
                month: 'long',
                day: '2-digit' 
              }).format(new Date(feed.createdDateTime))}
            </Feed.Date>
          </Feed.Summary>
          <Feed.Extra text>
            {feed.title}
          </Feed.Extra>
          <Feed.Meta>
            <Like 
              likes={feed.likes}
              totalLikes={feed.totalLikes}
            />
          </Feed.Meta>
        </Feed.Content>
      </Feed.Event>

    ) : <Skelton />;
    
    return (
      <Feed size="large">
          {feed}
      </Feed>
    );
  }
}

export default IdeaFeed;
