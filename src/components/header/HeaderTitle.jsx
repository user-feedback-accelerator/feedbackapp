import React, { Component } from 'react';
import './HeaderTitle.css';

class HeaderTitle extends Component {
  render() {
    const isAppContext = this.props.user ? true : false;

    return (
      <div className="feedback-header">
        {/* logo */}
        <div className="feedback-header-logo">
          {/* logo placeholder */}
        </div>
        {/* title */}
        <div className="feedback-header-title">
          <h1 className="ui blue header">
            {this.props.app} Idea Generator
            <i aria-hidden="true" className="lock icon" />
          </h1>
           { /* only if user details available */}
            {isAppContext ? (
              <h2>Welcome <span>{this.props.user}!</span></h2>
            ) : (
              <span></span>
           )} 
        </div>
        <div className="clear-float" />
      </div>
    );
  }
}

export default HeaderTitle;
