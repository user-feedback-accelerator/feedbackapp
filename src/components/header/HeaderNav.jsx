import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './HeaderNav.css';

class HeaderNav extends Component {
  render() {
    return (
      <div className="header-nav">
        <ul>
          <li>
            <Link to="/postidea">Post your idea</Link>
          </li>
          <li>
            <Link to="/getidea">Check out all the ideas</Link>
          </li>
          <li className="last-nav">
            <Link to="/whatsbuilt">What's getting built?</Link>
          </li>
        </ul>
        <div className="clear"></div>
      </div>
    );
  }
}

export default HeaderNav;
