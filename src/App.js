import React, { Component } from 'react';
import './App.css';
import 'semantic-ui-css/semantic.min.css';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import HeaderTitle from './components/header/HeaderTitle';
import HeaderNav from './components/header/HeaderNav';
import PageShell from './components/pageshell';
import PostIdea from './pages/postidea';
import GetIdea from './pages/getIdea';
import WhatsBuilt from './pages/whatsbuilt';
import Notfound from './pages/notfound';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      app: null,
      user: null
    }
  }
  componentDidMount() {
    // get url parameters
    const urlParams = new URLSearchParams(window.location.search);
    this.setState({
      app:urlParams.get('app'),
      user: urlParams.get('user'),
    })
  }

  render() {
    return (
      <Router>
        <div className="app-wrapper">
            <HeaderTitle 
              app={this.state.app}
              user={this.state.user} />
            <HeaderNav />
            <Switch>
              <Route exact path="/" component={PageShell(PostIdea)} />
              <Route exact path="/postidea" component={PageShell(PostIdea)} />
              <Route exact path="/getidea" component={PageShell(GetIdea)} />
              <Route exact path="/whatsbuilt" component={PageShell(WhatsBuilt)} />
              <Route component={Notfound}></Route>
            </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
